const PUZZLES = [
    "[A-Z]", // 2024-04-15
    "[aeiou]{3}",
    "^(.).*\\1$",
    "[a-c][x-z]",
    "f",
    "^[^mn]",
    "p.b",
    "q$|^w",
    "[nm].$",
    "o(..)*o",
    "^[aeiou]",
    "b..t",
    "[aeiou][^aeiou][aeiou][^aeiou][aeiou]",
    "n.?e",
    "r(a|o)l\\1",
    "(..)\\1",
    "^[^aeiou]",
    "[^A-Za-z]",
    "(.)(.)(.)\\3\\2\\1",
    "ess$",
    "ki|bo|ba",
    "(.)..\\1..\\1",
    "two|three|five|seven",
    "^(..)*.$",
    "(.)\\1",
    "^(.[aeiou].)*$",
    "ed$|ing$",
    "^.(.).*\\1.$",
    "^[a-c].*[x-z]$",

    "zz", // 2024-06-13
    "^[^eE]*$",
    "[^aeiou]$",
    "(.{2}).*\\1",
    "^([dn]).*\\1$",
    "^s.*s$",
    "^[^aeiou]*[aeiou][^aeiou]*$",
    "(...)\\1",
    "^.{3}$",
    "ly$",
    "(.)(.).*\\2\\1",
    "(.)(.).*\\2\\1",
    "^.{4}(.?){3}$",
    "^[a-m]+[n-z]+$",
    "[b-d][a-c]",
    "^(.).*(.).*\\2\\1$",
    "cat$",
    "^[aeiou]{2}",
    "one|two|three|four|five|six|seven|eight|nine",
    "[aeiou]{2}$",
    "s$",
    "^(.)(.).*\\2\\1$",
    "[^s]$",
    "(er)|(ing)",
    "[aeiou](.)\\1[aeiou]",
    "e",
    "(.).\\1",
    "(f).{2}\\1{2}y",
    "(...)\\1",
    "a{2}",
    "e.*e.*e.*e.*e",
    "(.+)(.+)\\1\\2",
    "^(.).*\\1$",
    "[a-zA-Z]{6,}",
    "^[a-m]*$",
    "^(.)a\\1",
    "\\W",
    "^(?!(..+)\\1+$)(.)\\2+$",
    "^[^asdfghjkl]*$",
    "o[t-z]",
    "(.)\\1",
    "^([^aeiou][aeiou])+$",
    "^a*b*c*d*e*f*g*h*i*j*k*l*m*n*o*p*q*r*s*t*u*v*w*x*y*z*$",
    "[aeiou]{2}",
    ".{5}",
    "([^aeiou])\\1",
    "^x",
    "^(.).*(.).*\\1\\2$",
    "^[^e]*$",
    "^.{7}$",
    "([aeiou])\\1+.*(ing|er)$",
    "e+.*",
    "^re",
    "[sh]{2}",
    "^[a-m].+[n-z]$",
    "[aiueo]{2}",
    "(([a-zA-Z])[^\\2]).*?(\\1)",
    "(.).+\\1",
    ".([a-z]{4})\\1",
    "tion",
    "(.{2}).*\\1",
    "sch",
    "mit$",
    "o.er",
    "(?:.*[A-Z]).*(...)\\1",
    "more|less",
    "a.*a",
    "ou?[^aeiou]",
    "^[^A-Z]",
    ".a.",
    "f.{5}r",
    "[^aeiou]{4}",
    "^[^aeiou]{2}",
    "ht",
    "(ly|lies)$",
    "e.t",
    "^Z.*a$",
    "nya",
    "(.)\\1.*(.)\\2",
    "^s.*s$",
    "(.{2}).*\\1",
    "^[^aA]*$",  // ca. 2024-09-03
]
